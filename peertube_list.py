import os
import shutil
import datetime

import requests
from bs4 import BeautifulSoup

url = 'http://peertube.net/new.php?p='
fp_folder = 'link_info'
fp_journal = ''
fp_head = fp_folder+os.path.sep+"video-latest.csv" #file path



    
def local(x):
    if x == 'g':
        return 1
    return 0

def get_videos(page):

    u = url + str(page)
    print "Getting:", u
    r = requests.get(u)
    if r.status_code == 200:
        url_list =[]
        c = r.content
        soup = BeautifulSoup(c)
        li = soup.find('div',class_='mostcontainer').find_all('ul')[0].find_all('li')

        for i in li:
            link = i.find('a')['href']
            duration = i.find('a').find('span',class_='time').get_text()
            viewcount = i.find('p',class_='viewcounts').get_text()
            img = i.find('img',class_='listing')['src']
            title = i.find('img',class_='listing')['alt']
            loc = local(i.find('img',class_='localavailability')['src'][7])
            url_list.append((link,duration,viewcount,img,title,loc))
            # print 
            # break
            # print >> f, link+","+duration+","+viewcount+","+img+","+title.encode('utf-8')+","+loc
    return url_list

def push_to_file(ll):

    global fp_folder 
    global fp_journal 
    global fp_head
    

    
    try:
        os.mkdir('link_info')
    except:
        print "directory already exists"
        pass

    f = open(fp_journal,'w')
    f.write("Link, Duration, Viewcount, Image_link, Title, localavailability\n")
    
    # for ll in url_list:
    for i in ll:
        line  =''
        for j in i:
            try:
                line = line + '"'+str(j)+ '",'
            except:
                print "skipping:" , j.encode('utf-8')
        print >> f, line[:-1]
    f.close()

    shutil.copy(fp_journal,fp_head)


def get_latest_pages(count):
    global dt_str
    global fp_journal

    dt_str = str(datetime.datetime.now()).replace(":","-").replace('.','-')
    fp_journal = fp_folder+os.path.sep+"video-"+dt_str+".csv" #file path

    ll = []
    for i in range(1,count+1):
        ll = ll+get_videos(i)


    print len(ll)
    assert len(ll) == 24 * count
    push_to_file(ll)

if __name__ == '__main__':


    get_latest_pages(5)